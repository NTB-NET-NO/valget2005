Imports System.Xml.Xsl
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected WithEvents Xml1 As System.Web.UI.WebControls.Xml
    Protected WithEvents Xml2 As System.Web.UI.WebControls.Xml


    Const ERR_FILE As String = "EMPTY.xml"
    'Const XSL_DETAIL As String = "xslt\Valg-xhtml_v2.xsl"
    Const XSL_DETAIL As String = "xslt\Valg2005-xhtml.xsl"
    'Const XSL_FRAMMOTE As String = "xslt\Valg-xhtml-frammote.xsl"
    Const XSL_FRAMMOTE As String = "xslt\Valg2005-xhtml-frammote.xsl"
    'Const XSL_FYLKE As String = "xslt\Valg-fylke-xhtml.xsl"
    Const XSL_FYLKE As String = "xslt\Valg2005-fylke-xhtml.xsl"
    'Const XSL_LISTE_BY As String = "xslt\byliste.xsl"
    Const XSL_LISTE_BY As String = "xslt\Valg2005-byliste.xsl"
    'Const XSL_VG_LISTE As String = "xslt\Valg-fylke-kommune-VG-xhtml.xsl"
    Const XSL_VG_LISTE As String = "xslt\Valg2005-fylke-kommune-VG-xhtml.xsl"
    Private toNotabene As Boolean

    Public buttons As Boolean = True

    Private bydelOslo As String = "no"
    Private xmlpath As String
    Private xml_F01 As String
    Private xml_F03 As String 
    Private xml_F05 As String 

    Private xml_K01 As String 
    Private xml_K03 As String 
    Private xml_K07 As String 
    Protected WithEvents Checkbox2 As System.Web.UI.WebControls.CheckBox
    Private xml_K09 As String
    'Stortings rapporter
    Private xml_ST06 As String ' Landsoversikt storting


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Initier()
        xmlpath = Session("xmlpath")

        'xml_F01 = xmlpath & "\F01.xml"
        xml_F01 = xmlpath & "\ST04.xml"

        'xml_F03 = xmlpath & "\F13.xml"
        xml_F03 = xmlpath & "\ST03.xml"

        xml_F05 = xmlpath & "\F05.xml"
        xml_ST06 = xmlpath & "\ST06.xml" 'replaces F05

        'xml_K01 = xmlpath & "\K01.xml"
        xml_K01 = xmlpath & "\ST04.xml"

        xml_K03 = xmlpath & "\K13.xml"

        'xml_K07 = xmlpath & "\K07.xml"
        xml_K07 = xmlpath & "\ST14.xml"

        xml_K09 = xmlpath & "\K09.xml"



        'Me.Xml1.DocumentSource = xml_F05
        Me.Xml1.DocumentSource = xml_ST06
        Me.Xml2.DocumentSource = xml_F01
        Me.Xml3.DocumentSource = xml_F03
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Initier()
        Dim xmlFile As String
        Dim argList2 As XsltArgumentList = New XsltArgumentList()
        Dim argList1 As XsltArgumentList = New XsltArgumentList()

        toNotabene = Request.UserHostAddress = "127.0.0.1" _
            Or Request.UserHostAddress.IndexOf("193.69.125.") > -1

        Dim sperret As Boolean = Session("sperret")
        If Not toNotabene And sperret Then
            'Brukere utenfor NTBs domene
            Response.Redirect("sperrefrist.htm")
            Exit Sub
        End If

        'debug:
        'Dim test
        'For Each test In Request.Form
        '    Response.Write(test & "; ")
        'Next

        Dim tabelltype As String = Request.QueryString("tabelltype")
        If tabelltype <> "" Then
            argList1.AddParam("tabelltype", "", tabelltype)
        End If

        If IsPostBack = True Then
            Session("status") = Me.CheckBox1.Checked
        Else
            Me.CheckBox1.Checked = Session("status")
        End If

        Dim status As Boolean = Session("status")
        argList1.AddParam("status", "", status)


        Dim valgType As String = Request.QueryString("valgType")
        If valgType = "" Then
            valgType = Session("valgType")
            If valgType = "" Then
                'valgType = "F"
                valgType = "St"
            End If
        Else
            Session("valgType") = valgType
        End If

        If valgType = "F" Then
            Me.Xml2.DocumentSource = xml_F01
            Me.Xml3.DocumentSource = xml_F03
            Me.Label1.Text = "Stortingsvalget" 'Me.Label1.Text = "Fylkestingsvalget"
            Me.Label3.Text = "<a href='Default.aspx?valgType=F'>Landsoversikt</a>"
        ElseIf valgType = "St" Then
            Me.Xml2.DocumentSource = xml_F01
            Me.Xml3.DocumentSource = xml_F03
            Me.Label1.Text = "Stortingsvalget"
            Me.Label3.Text = "<a href='Default.aspx?valgType=St'>Landsoversikt</a>"
        Else
            Me.Xml2.DocumentSource = xml_F01 'Me.Xml2.DocumentSource = xml_K01
            Me.Xml3.DocumentSource = xml_F03 'Me.Xml3.DocumentSource = xml_K03
            Me.Label1.Text = "Stortingsvalget" 'Me.Label1.Text = "Kommunevalget"
            'Me.Label3.Text = "<a href='Default.aspx?valgType=K'>Landsoversikt</a>"
            Me.Label3.Text = "<a href='Default.aspx?valgType=St'>Landsoversikt</a>"
        End If

        Dim fylkeNr As String = Request.QueryString("FylkeNr")
        Dim liste As String = Request.QueryString("liste")
        Dim kommuneNr As String = Request.QueryString("KommuneNr")
        Dim byNr As String = Request.QueryString("byNr")
        Dim kretsNr As String = Request.QueryString("kretsNr")
        Dim oversiktFylkeNr As String = Request.QueryString("OversiktFylkeNr") & ""
        Dim expand As String = Request.QueryString("Expand") & ""
        argList2.AddParam("Expand", "", expand)


        If fylkeNr = "03" Then
            'valgType = "K" 'change to stortingsvalg
            valgType = "St"
        End If

        Dim partiKategori As Integer = Session("partiKategori")
        Dim partiKategoriForm As Integer = Request.Form("DropDownList1")

        If partiKategoriForm <> 0 Then
            partiKategori = partiKategoriForm
        End If

        If partiKategori = 0 Then
            partiKategori = 1
        End If

        Session("partiKategori") = partiKategori
        Me.DropDownList1.SelectedIndex = partiKategori - 1
        argList1.AddParam("Partikategori", "", partiKategori)
        argList1.AddParam("ntbdato", "", Format(Now, "dd.MM.yyyy HH:mm"))

        Dim navn As String = Request.QueryString("Navn")
        Me.Xml1.TransformSource = XSL_DETAIL
        Me.Xml1.TransformArgumentList = argList1

        Dim argList3 As XsltArgumentList = New XsltArgumentList
        argList3.AddParam("valgtype", "", valgType)
        If byNr <> "" Then
            argList3.AddParam("ByNr", "", byNr)
        End If
        Me.Xml3.TransformArgumentList = argList3

        If oversiktFylkeNr <> "" Or valgType = "M" Or liste = "07" Then
            ' Skjuler buttons for nedlasting av spesialformater
            buttons = False
        End If

        'Response.Write(FylkeNr)
        If valgType = "M" Then
            Me.Xml1.DocumentSource = xml_K07
            Me.Xml1.TransformSource = XSL_FRAMMOTE
            'Session("valgType") = "K"
            Session("valgType") = "St"
        ElseIf liste = "oslo" Then
            Me.Xml1.DocumentSource = xml_K09
            Me.Xml1.TransformSource = XSL_DETAIL

        ElseIf oversiktFylkeNr <> "" Then
            'Dim xmlFile As String = XML_K01
            'CheckXmlDoc(xmlFile, navn)
            'Dim argList As XsltArgumentList = New XsltArgumentList()

            'Me.Checkbox2.Visible = True 'IKKE BRUK BYDEL i Valg2005

            If Me.Checkbox2.Checked Then
                argList1.AddParam("bydel", "", "yes")
            End If

            ' argList1.AddParam("bydel", "", "yes")
            argList1.AddParam("FylkeNr", "", oversiktFylkeNr)
            argList1.AddParam("xmlpath", "", xmlpath)

            Me.Xml1.DocumentSource = xml_K01
            Me.Xml1.TransformSource = XSL_VG_LISTE
            Me.Xml1.TransformArgumentList = argList1

        ElseIf liste = "07" Then
            xmlFile = xmlpath & "\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
            Me.Xml1.TransformSource = XSL_FYLKE
        ElseIf liste <> "" Then
            xmlFile = xmlpath & "\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kretsNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "03-" & byNr & "-" & kretsNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kommuneNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "02-" & kommuneNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            If fylkeNr <> "" Then
                'Dim argList As XsltArgumentList = New XsltArgumentList()
                argList2.AddParam("FylkeNr", "", fylkeNr)
                Me.Xml2.TransformArgumentList = argList2
            End If
        ElseIf fylkeNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "04-" & fylkeNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            argList2.AddParam("FylkeNr", "", fylkeNr)
            Me.Xml2.TransformArgumentList = argList2
        Else
            If valgType = "K" And partiKategori = 3 Then
                partiKategori = 2
                argList1.RemoveParam("Partikategori", "")
                argList1.AddParam("Partikategori", "", partiKategori)
                Me.DropDownList1.SelectedIndex = partiKategori - 1
                'Me.DropDownList1.Enabled = False
            End If

            'endret fra valg2003 til valg2005
            xmlFile = xmlpath & "\" & valgType & "06" & ".xml"
            'xmlFile = xmlpath & "\" & valgType & "05" & ".xml"
            CheckXmlDoc(xmlFile, navn)
        End If

        If toNotabene Then
            Try
                Dim test As String = Path.GetFileNameWithoutExtension(xmlFile).Substring(0, 3)
                Dim test2 As String = Path.GetFileNameWithoutExtension(xmlFile)
                If test = "F04" Or test = "F05" Or test2 = "K02-0301" Then
                    Me.Button1.Visible = True
                End If
            Catch
            End Try
        End If

        Session("xmlFile") = Me.Xml1.DocumentSource
        Session("xsltFile") = Me.Xml1.TransformSource
        Session("argList") = Me.Xml1.TransformArgumentList

        'Me.Label2.Text = Request.UserHostAddress

    End Sub

    Private Sub CheckXmlDoc(ByVal xmlFile As String, ByVal navn As String)
        Dim testFile As String = Server.MapPath(xmlFile)
        If IO.File.Exists(testFile) Then
            Me.Xml1.DocumentSource = xmlFile
        Else
            Dim argList As XsltArgumentList = New XsltArgumentList()
            argList.AddParam("navn", "", navn)
            Me.Xml1.TransformArgumentList = argList
            Me.Xml1.DocumentSource = ERR_FILE
            Me.Label2.Text = xmlFile
            toNotabene = False
        End If
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
        Response.Write("Error")
    End Sub

    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Xml3 As System.Web.UI.WebControls.Xml
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Button3 As System.Web.UI.WebControls.Button
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button
    Protected WithEvents Button4 As System.Web.UI.WebControls.Button

    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim xmlFile As String = Session("xmlFile")
        Dim xmlFile2 As String = Path.GetFileNameWithoutExtension(xmlFile)
        xmlFile = Server.MapPath(xmlFile)
        Dim strTemp1 As String = DoXstlTransform(xmlFile, Server.MapPath("xslt/Valg2notabene-HTML.xsl"))
        WriteFile(Server.MapPath("valg-til-notabene\" & xmlFile2 & ".htm"), strTemp1)
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub
End Class
